﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebChat.Server.Models.SqlDbEntities;

namespace WebChat.Server.Models.Api
{
    public class ConversationMember
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }

        public ConversationMember()
        {
        }

        public ConversationMember(int id, string username, string email, bool isActive)
        {
            Id = id;
            Username = username;
            Email = email;
            IsActive = isActive;
        }

        public ConversationMember(AppUser user, bool isActive)
        {
            Id = user.Id;
            Username = user.Username;
            Email = user.Email;
            IsActive = isActive;
        }
    }
}
