﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebChat.Server.Models.Api
{
    public class SignInCredentials
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
