﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using WebChat.Server.Models.Api;

namespace WebChat.Server.Models.SqlDbEntities
{
    public class AppUser
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        // TODO hash passwords
        [JsonIgnore]
        public string Password { get; set; }

        // Connect/Disconnect do not have to be related to SignalR Hub connection.
        // They just indicate a user's (in)activity and can, for example, be set
        // by hand after a period of no input from user.
        public DateTime? LastConnect { get; set; }
        public DateTime? LastDisconnect { get; set; }

        [JsonIgnore]
        public virtual List<RefreshToken> RefreshTokens { get; set; } = new List<RefreshToken>();
        [JsonIgnore]
        public virtual List<Conversation> Conversations { get; set; } = new List<Conversation>();
        [JsonIgnore]
        [InverseProperty(nameof(Conversation.CreatedBy))]
        public virtual List<Conversation> CreatedConversations { get; set; } = new List<Conversation>();
        [JsonIgnore]
        public virtual List<Message> Messages { get; set; } = new List<Message>();

        public AppUser()
        {
        }

        public AppUser(RegistrationCredentials credentials)
        {
            Username = credentials.Username;
            Email = credentials.Email;
            Password = credentials.Password;
        }
    }
}
