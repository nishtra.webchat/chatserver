﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebChat.Server.Hubs;
using WebChat.Server.Models.Api;
using WebChat.Server.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebChat.Server.Controllers
{
    [Route("api")]
    [ApiController]
    [Authorize]
    public class ChatApiController : ControllerBase
    {
        private readonly IChatDbService chatDbService;
        
        public ChatApiController(IChatDbService chatDbService)
        {
            this.chatDbService = chatDbService;
        }
        
        /// <summary>
        /// Get conversations for the user indicated in the JWT that came with the request.
        /// As such works only for authenticated users.
        /// </summary>
        /// <returns>Collection of conversations</returns>
        [HttpGet("user-conversations")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<ConversationListItem>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorResponse))]
        public IActionResult GetUserConversations()
        {
            var callerUserId = Helpers.GetUserIdFromClaims(HttpContext.User);
            if (callerUserId == null)
                return BadRequest(new ErrorResponse("Bad request! User is unauthorized or doesn't have required claims!"));

            var conversations = chatDbService.GetConversationsForUser(callerUserId.Value);
            return Ok(conversations);
        }

        /// <summary>
        /// Search the DB for conversations with a matching name. Works only if the correct JWT was provided in the request.
        /// </summary>
        /// <param name="searchQuery">Search string</param>
        /// <returns>Collection of conversations</returns>
        [HttpGet("find")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<ConversationListItem>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorResponse))]
        public IActionResult FindConversations([FromQuery(Name = "search-query")] string searchQuery)
        {
            var callerUserId = Helpers.GetUserIdFromClaims(HttpContext.User);
            if (callerUserId == null)
                return BadRequest(new ErrorResponse("Bad request! User is unauthorized or doesn't have required claims!"));

            var searchResults = chatDbService.FindUsersAndConversations(searchQuery, callerUserId.Value);
            return Ok(searchResults);
        }

        /// <summary>
        /// Creates and saves to the DB a new conversation. Works only if the correct JWT was provided in the request.
        /// </summary>
        /// <param name="groupName">A name for the new conversation</param>
        /// <returns>Created conversation</returns>
        [HttpPost("create-conversation")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ConversationListItem))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorResponse))]
        public IActionResult CreateConversation([FromBody] string groupName)
        {
            var callerUserId = Helpers.GetUserIdFromClaims(HttpContext.User);
            if (callerUserId == null)
                return BadRequest(new ErrorResponse("Bad request! User is unauthorized or doesn't have required claims!"));
            if (String.IsNullOrWhiteSpace(groupName) || groupName.StartsWith("@"))
                return BadRequest(new ErrorResponse("Invalid group name"));

            var conversation = chatDbService.CreateGroupChat(callerUserId.Value, groupName);
            if (conversation == null)
                return BadRequest(new ErrorResponse("Couldn't create a conversation. Conversation with this name alredy exists."));

            var strippedConversationItem = new ConversationSearchItem(conversation);
            return Ok(strippedConversationItem);
        }

        /// <summary>
        /// Removes the user indicated in the JWT data from the members of a given conversation.
        /// Works only if the correct JWT was provided in the request.
        /// </summary>
        /// <param name="conversationId">Conversation the user has left</param>
        /// <returns></returns>
        [HttpPost("leave-conversation")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorResponse))]
        public IActionResult LeaveConversation([FromBody] int conversationId)
        {
            var callerUserId = Helpers.GetUserIdFromClaims(HttpContext.User);
            if (callerUserId == null)
                return BadRequest(new ErrorResponse("Bad request! User is unauthorized or doesn't have required claims!"));

            chatDbService.LeaveConversation(conversationId, callerUserId.Value);
            return Ok();
        }

        /// <summary>
        /// Get the latest messages for the given conversation.
        /// </summary>
        /// <param name="groupId">Conversation ID</param>
        /// <param name="numberOfMessages">A number of messages to load</param>
        /// <returns>Collection of the messages</returns>
        [HttpGet("latest-messages")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<MessageDataToClient>))]
        public IActionResult GetLatestMessagesForConversation(
            [FromQuery(Name = "groupid")] int groupId,
            [FromQuery(Name = "n")] int numberOfMessages)
        {
            if (numberOfMessages <= 0)
                numberOfMessages = 10;

            var messages = chatDbService.GetLastNMessages(groupId, numberOfMessages);
            var responseData = messages.Select(m => new MessageDataToClient(m));

            return Ok(responseData);
        }

        /// <summary>
        /// Get the messages sent before a certain time for the given conversation.
        /// </summary>
        /// <param name="groupId">Conversation ID</param>
        /// <param name="numberOfMessages">A number of messages to load</param>
        /// <param name="beforeTime">Search messages before this time</param>
        /// <returns>Collection of the messages</returns>
        [HttpGet("prev-messages")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<MessageDataToClient>))]
        public IActionResult GetPreviousMessagesForConversation(
            [FromQuery(Name = "groupid")] int groupId,
            [FromQuery(Name = "n")] int numberOfMessages,
            [FromQuery(Name = "beforetime")] DateTime beforeTime)
        {
            if (numberOfMessages <= 0)
                numberOfMessages = 10;

            var messages = chatDbService.GetPrevNMessages(groupId, numberOfMessages, beforeTime);
            var responseData = messages.Select(m => new MessageDataToClient(m));

            return Ok(responseData);
        }

        /// <summary>
        /// Get the member users of a given conversation.
        /// </summary>
        /// <param name="groupId">Conversation ID</param>
        /// <returns>Collection of conversation members</returns>
        [HttpGet("members")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<ConversationMember>))]
        public IActionResult GetConversationMembers(
            [FromQuery(Name = "groupid")] int groupId)
        {
            var members= chatDbService.GetUsersInConversation(groupId);
            return Ok(members);
        }

        /// <summary>
        /// Get the data of a given conversation and update it's user activity.
        /// Works only if the correct JWT was provided in the request.
        /// </summary>
        /// <param name="groupId">Conversation the user has opened</param>
        /// <returns>Conversation data</returns>
        [HttpGet("conversation-connect")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ChatRoomData))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorResponse))]
        public IActionResult ConnectToConversation(
            [FromQuery(Name = "groupid")] int groupId)
        {
            var userId = Helpers.GetUserIdFromClaims(HttpContext.User);
            if (userId == null)
                return BadRequest(new ErrorResponse("Bad request! User is unauthorized or doesn't have required claims!"));

            
            // get latest messages
            int numberOfMessages = 10;
            var latestMessages = chatDbService
                .GetLastNMessages(groupId, numberOfMessages)
                .Select(m => new MessageDataToClient(m));
            
            // update user activity status in DB
            chatDbService.RegisterUserConnectingToConversation(userId.Value, groupId);

            // get conversation members
            var members = chatDbService.GetUsersInConversation(groupId);
            
            var responseData = new ChatRoomData() {
                LatestMessages = latestMessages,
                Members = members
            };
            return Ok(responseData);
        }


        /// <summary>
        /// Register the end of the user's activity in the given conversation.
        /// Works only if the correct JWT was provided in the request.
        /// </summary>
        /// <param name="groupId">Conversation the user has closed</param>
        /// <returns></returns>
        [HttpGet("conversation-disconnect")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorResponse))]
        public IActionResult DisconnectFromConversation(
            [FromQuery(Name = "groupid")] int groupId)
        {
            var userId = Helpers.GetUserIdFromClaims(HttpContext.User);
            if (userId == null)
                return BadRequest(new ErrorResponse("Bad request! User is unauthorized or doesn't have required claims!"));

            chatDbService.RegisterUserDisconnectingFromConversation(userId.Value, groupId);
            return Ok();
        }

        
    }
}
