﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebChat.Server.Controllers
{

    [ApiController]
    [Route("/")]
    public class TestController : Controller
    {
        /// <summary>
        /// Indicates on the server root URL that it is working
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public string Index()
        {
            return "The chat server is up and running. To interface with the server use public API.";
        }
    }
}
