using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Npgsql;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebChat.Server.DataAccess;
using WebChat.Server.Hubs;
using WebChat.Server.Services;

namespace WebChat.Server
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            var builder = new ConfigurationBuilder();
            builder
                .AddConfiguration(configuration)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connStr = Configuration.GetConnectionString("postgresdb");
            services.AddDbContext<AppDbContext>(options => options.UseNpgsql(connStr));
            services.AddScoped<DbDataInitializer>();

            services.AddScoped<IAuthService, SqlDbAuthService>();
            services.AddScoped<IChatDbService, SqlChatDbService>();

            services.AddCors(options =>
            {
                // TODO configure CORS for prod environment
                options.AddPolicy("CorsPolicy",
                    builder => builder.WithOrigins("http://localhost:8080", "https://*.gitlab.io")
                    .SetIsOriginAllowedToAllowWildcardSubdomains()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options => AuthOptions.ConfigureJwtBearer(options));

            services.AddSignalR();
            services.AddSingleton<IUserIdProvider, SignalrEmailBasedUserIdProvider>();

            services.AddControllers();
            services.AddHostedService<TokenCleanerService>();

            services.AddSwaggerGen(c => {
                {
                    var filePath = Path.Combine(AppContext.BaseDirectory, "WebChat.Server.xml");
                    c.IncludeXmlComments(filePath);
                }
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            InitializeDb(app); // seed DB data

            app.UseRouting();

            app.UseCors("CorsPolicy");

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
                endpoints.MapHub<ChatHub>("/chat");
            });
        }

        private void InitializeDb(IApplicationBuilder app)
        {
            var scopeFactory = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>();
            using (var scope = scopeFactory.CreateScope())
            {
                var dbInitializer = scope.ServiceProvider.GetService<DbDataInitializer>();
                dbInitializer.Initialize();
                dbInitializer.SeedData();
            }
        }
    }
}
